import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";

export enum TestEnum {
  ENUM1 = "ENUM1",
  ENUM2 = "ENUM2"
}

export declare class CustomType2 {
  readonly field3?: string;
  readonly testCustomFieldEnum?: TestEnum | keyof typeof TestEnum;
  constructor(init: ModelInit<CustomType2>);
}

export declare class CustomType {
  readonly field1?: string;
  readonly field2: boolean;
  readonly fieldCustomType?: CustomType2;
  constructor(init: ModelInit<CustomType>);
}

type AppointmentMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

export declare class Appointment {
  readonly id: string;
  readonly address: string;
  readonly phoneNumber: string;
  readonly date?: string;
  readonly time1?: string;
  readonly time2?: string;
  readonly status?: string;
  readonly oldId?: string;
  readonly type?: string;
  readonly brand?: string;
  readonly color?: string;
  readonly comments?: string;
  readonly partsCost?: number;
  readonly totalEarned?: number;
  readonly customerComplain?: string;
  readonly testInt?: number;
  readonly testBoolean?: boolean;
  readonly testDateTime?: string;
  readonly testTimeStamp?: number;
  readonly testEmail?: string;
  readonly testJSON?: string;
  readonly testUR?: string;
  readonly testPhone?: string;
  readonly testIPAddress?: string;
  readonly testEnum?: TestEnum | keyof typeof TestEnum;
  readonly testCustomType?: CustomType2;
  readonly testStringArray?: (string | null)[];
  readonly createdAt?: string;
  readonly updatedAt?: string;
  constructor(init: ModelInit<Appointment, AppointmentMetaData>);
  static copyOf(source: Appointment, mutator: (draft: MutableModel<Appointment, AppointmentMetaData>) => MutableModel<Appointment, AppointmentMetaData> | void): Appointment;
}