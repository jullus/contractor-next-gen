// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';

const TestEnum = {
  "ENUM1": "ENUM1",
  "ENUM2": "ENUM2"
};

const { Appointment, CustomType2, CustomType } = initSchema(schema);

export {
  Appointment,
  TestEnum,
  CustomType2,
  CustomType
};